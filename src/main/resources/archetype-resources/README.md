## Generate the project 

The result is a simple project that shows the basics of configuring the WebServer and implementing basic 
routing rules.

Run the Maven archetype
```bash
mvn archetype:generate -DinteractiveMode=false \ 
     -DarchetypeGroupId=org.flvs.microservices.archetypes \
     -DarchetypeArtifactId=microservices.template \
     -DarchetypeVersion=1.0-SNAPSHOT \
     -DgroupId=org.flvs.microservices \
     -DartifactId=hello-service \
     -Dpackage=org.flvs -Ddatabase=teste
```

## Build and run

With JDK11+
```bash
cd archetype-jpa-microservice
mvn package
java -jar target/${artifactId}.jar
```

## Exercise the application

```
curl -X GET http://localhost:8090/greet
{"message":"Hello World!"}
```

## Try health and metrics

```
curl -s -X GET http://localhost:8090/health
{"outcome":"UP",...
. . .

# Prometheus Format
curl -s -X GET http://localhost:8090/metrics
# TYPE base:gc_g1_young_generation_count gauge
. . .

# JSON Format
curl -H 'Accept: application/json' -X GET http://localhost:8090/metrics
{"base":...
. . .
```
## Start Mysql

Open stack.yml and change 

- /Users/fabio/dockerimagesdata/databaseimagesdata/mysql:/var/lib/mysql

to your mysql data directory

- `<your mysql data directory>`/mysql:/var/lib/mysql

run docker composer 

```bash
chmod 755 up.sh 
./up.sh
```

Create User schema and table
```bash
mvn flyway:migrate
```

## Try the database connection

```
# All users
curl -X GET http://localhost:8090/users
[{"id":1,"name":"Huguinho"},{"id":2,"name":"Zezinho"},{"id":3,"name":"Luizinho"}]

. . .

# User Id 1
curl -X GET http://localhost:8090/users/1
{"id":1,"name":"Huguinho"}

```




