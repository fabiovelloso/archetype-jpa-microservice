package ${package}.service.fitnessfunctions;

import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.core.importer.ClassFileImporter;
import com.tngtech.archunit.core.importer.ImportOption;
import com.tngtech.archunit.lang.ArchRule;
import com.tngtech.archunit.lang.syntax.ArchRuleDefinition;
import com.tngtech.archunit.library.Architectures;
import org.junit.jupiter.api.Test;

public class ArcTestArchT {

    JavaClasses importedClasses = new ClassFileImporter()
            .withImportOption(ImportOption.Predefined.DO_NOT_INCLUDE_TESTS)
            .withImportOption(ImportOption.Predefined.DO_NOT_INCLUDE_ARCHIVES)
            .importPackages("org.flvs");

    @Test
    public void some_architecture_rule() {

        ArchRule rule = ArchRuleDefinition.classes()
                .that().resideInAPackage("..infrastructure..")
                .should().onlyBeAccessed()
                .byAnyPackage( "..domain..");
//        ArchRule rule = classes().that().resideInAPackage("..foo..")
//                .should().onlyHaveDependentClassesThat().resideInAnyPackage("..source.one..", "..foo..")

      // rule.check(importedClasses);
    }

    @Test
    public void hexagonal_architecture_rule() {


        Architectures.LayeredArchitecture portsAndAdaptersArchitecture =
                Architectures
                        .layeredArchitecture()
                        .layer("domain layer")
                        .definedBy("com.company.app.domain..")
                        .layer("application layer")
                        .definedBy("com.company.app.application..")
                        .layer("adapters layer")
                        .definedBy("com.company.app.adapters..");

        ArchRule applicationLayerRule =
                portsAndAdaptersArchitecture
                        .whereLayer("application layer")
                        .mayOnlyBeAccessedByLayers("adapters layer");

       // applicationLayerRule.check(importedClasses);
    }


}
