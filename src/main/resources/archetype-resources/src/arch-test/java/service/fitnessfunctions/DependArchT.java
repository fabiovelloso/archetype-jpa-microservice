package ${package}.service.fitnessfunctions;

import jdepend.framework.JDepend;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;


public class DependArchT {

    @Test
    public void testCycles() throws IOException {
        JDepend jdepend = new JDepend();
        jdepend.addDirectory("target/classes");

        jdepend.analyze();
        Assertions.assertTrue(jdepend.containsCycles() == false);
       // assertThat(jdepend.containsCycles(), is(false));
    }

//    @Test
//    public void some_architecture_rule() {
//        double ideal = 0;
//        double tolerance = 0.6;
//        JDepend jdepend = new JDepend();
//        try {
//            jdepend.addDirectory("target/classes/org/flvs/service");
//            Collection packages = jdepend.analyze();
//            Iterator iterator = packages.iterator();
//            while (iterator.hasNext()) {
//                JavaPackage p = (JavaPackage) iterator.next();
//                System.out.println(p.getName());
//                Assertions.assertEquals(ideal,p.distance(),tolerance, "Distance exceded:" + p.getName());
//            }
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }

}
