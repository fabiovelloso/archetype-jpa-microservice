package ${package}.service;

import io.helidon.microprofile.server.Server;
import io.restassured.RestAssured;
import io.restassured.parsing.Parser;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import javax.enterprise.inject.se.SeContainer;
import javax.enterprise.inject.spi.CDI;

import static org.hamcrest.core.IsEqual.equalTo;

class UserTestIT {

    private static Server server;

    @BeforeAll
    public static void startTheServer() throws Exception {
        RestAssured.defaultParser = Parser.JSON;
        server = Server.create().start();
    }


    @Test
    public void givenUrl_whenSuccessOnGetsResponseAndJsonHasRequiredKV_thenCorrect() {
        RestAssured.given().
                when().
                get(getConnectionString("/users/1")).
                then().
                statusCode(200).
                body("name", equalTo("Huguinho"));
    }

    @Test
    public void givenUrl__thenNo_Content() {
        RestAssured.given().
                when().
                get(getConnectionString("/users/29637451")).
                then().
                statusCode(204);
    }


    @AfterAll
    static void destroyClass() {
        CDI<Object> current = CDI.current();
        ((SeContainer) current).close();
    }

    private String getConnectionString(String path) {
        return "http://localhost:" + server.port() + path;
    }
}
