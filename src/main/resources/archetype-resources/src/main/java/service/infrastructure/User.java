package ${package}.service.infrastructure;

import javax.persistence.*;

@Entity(name = "User")
@Table(name = "User")
public class User {

    @Id
    @Column(name = "id", updatable = false, nullable = false)
    @GeneratedValue(strategy=GenerationType.AUTO, generator="user_sequence")
    @SequenceGenerator(name="user_sequence", sequenceName="user_seq")
    private int id;

    @Column(name = "name")
    private String name;

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

}
