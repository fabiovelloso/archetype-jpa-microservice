package ${package}.service.application;


import ${package}.service.infrastructure.User;
import ${package}.service.infrastructure.UserRepository;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import jakarta.transaction.Transactional;
import java.util.List;

@ApplicationScoped
@Transactional
public class UserApplicationService {

    @Inject
    private UserRepository repository;

    public List<User> findAll() {
       return repository.findAll();
    }

    public User findBy(int id) {
        return repository.findBy(id);
    }

    public void remove(int id) {
        repository.removeAndFlush(repository.findBy(id));
    }

}
