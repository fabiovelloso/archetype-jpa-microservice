package org.flvs.service.interfaces.books;

import javax.json.Json;
import javax.json.JsonBuilderFactory;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.Collections;

@Path("/books")
public class BookResource {

    private static final JsonBuilderFactory JSON = Json.createBuilderFactory(Collections.emptyMap());

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Book getBook(@Valid @NotNull @QueryParam("id") Long id) {
      Book book = new Book();
      book.setId(id);
      book.setAuthor("Fabio Velloso");
      book.setPages(100);
      return book;
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Book createBook(@Valid Book book) {
        book.setId(20L);
        return book;
    }

}
