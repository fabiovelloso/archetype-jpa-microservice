package ${package}.service.interfaces;

import ${package}.service.application.UserApplicationService;
import ${package}.service.infrastructure.User;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/users")
public class UserResource {

    @Inject
    private UserApplicationService userApplicationService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response findAll() {
        return Response.ok(userApplicationService.findAll()).build();
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response findById(@PathParam("id") int id) {
        User track = userApplicationService.findBy(id);
        return track == null ? Response.status(Response.Status.NO_CONTENT).build() : Response.ok(track).build();
    }

    @DELETE
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response delete(@PathParam("id") int id) {
        userApplicationService.remove(id);
        return Response.ok("OK "+ id).build();
    }

//    @PUT
//    @Path("/decrease/{id}")
//    @Produces(MediaType.APPLICATION_JSON)
//    public Response getMessage(@PathParam("id") int id) {
//        System.out.println("PUT");
//        return Response.ok(userApplicationService.decrease(id)).build();
//    }

}
