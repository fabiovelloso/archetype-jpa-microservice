CREATE DATABASE ${database};
USE ${database};
CREATE TABLE USER (
	id INT NOT NULL AUTO_INCREMENT,
    name VARCHAR (30) NOT NULL,
	PRIMARY KEY (id));

insert into USER (name) values ('Huguinho');
insert into USER (name) values ('Zezinho');
insert into USER (name) values ('Luizinho');
 @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "user_gen")
    @SequenceGenerator(name="user", sequenceName="user_seq")

